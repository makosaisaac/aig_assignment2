### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 310e6cf0-cf7e-11eb-0eb5-b71bc39589af
using Pkg;

# ╔═╡ 0636c81b-775c-46fe-a6f2-b09eaa2724f7
Pkg.activate(".")

# ╔═╡ 61e6b54f-cd9c-4e2c-bb60-e1f104f34e4a
begin
	using Flux;
	using MLDatasets: MNIST
	using Flux.Data: DataLoader
	using Flux: onehotbatch, logitcrossentropy, params, update!, train!, onecold, @epochs, onehot
	using Plots
	using PlutoUI
	using Printf
	using Statistics
	using EvalMetrics
end

# ╔═╡ 56c8f34e-442d-4bba-9680-c6c3f502e749
using PrettyTables	

# ╔═╡ b3d39712-d633-4e5a-8658-3dad0cf652c6
using Images,FileIO,ImageMagick,ImageIO

# ╔═╡ 6e1cce7d-bdfa-4fbd-a62b-9e438d5bb1b5
# Pkg.add("PlutoUI"); Pkg.add("Flux"); Pkg.add("Plots"); Pkg.add("EvalMetrics"); Pkg.add("Plotly"); Pkg.add("PrettyTables"); Pkg.add("MLDatasets"); Pkg.add("Images"); Pkg.add("FileIO"); Pkg.add("ImageMagick"); Pkg.add("ImageIO"); Pkg.add("ImageTransformations")

# ╔═╡ bc25b53a-af0b-498c-8f7a-5fc92d656cc3
md"### Loading and Declaring Training and Testing Dataset"

# ╔═╡ 94ba899d-f4ad-49e6-9cb9-e21941037ae7
md"#### File Path"

# ╔═╡ b5c6b738-0ddd-4121-8070-08423b664168
begin
	trainPath = "C:\\Users\\Isaac Makosa\\Desktop\\Assignment2\\chest_xray\\train"
	testPath = "C:\\Users\\Isaac Makosa\\Desktop\\Assignment2\\chest_xray\\test"
end

# ╔═╡ aeaf71df-8d82-477e-84b2-7873820b9fbd
labels=["NORMAL","PNEUMONIA"]

# ╔═╡ 3844a430-4476-487d-aa0f-4e7cf43b266a
md"### TRAINING DATASETS"

# ╔═╡ ae1fad32-8601-4fc9-8217-d3b1d878de68
train_dir = readdir(trainPath)

# ╔═╡ 8b5b2a5f-a953-4a29-873e-5f0248d1d001
trainData = []

# ╔═╡ fc0f045a-852d-461c-a4fc-f8f4eb8b0b2b
trainLabels = []

# ╔═╡ b8a5131b-ffcf-479c-ac3e-a9b64e3af858
for trn in train_dir
	if trn == "NORMAL"
		nom = joinpath(trainPath,trn)
		for alltrainImg in readdir(nom)
			# println(alltrainImg)
			# println(joinpath(nom,alltrainImg))
			img = load(joinpath(nom,alltrainImg))
			img = Gray.(img)
			img = imresize(img,(28,28))
			# img = convert(Array{Float64},img)
			push!(trainData,img)
			push!(trainLabels,trn)
		end
	elseif trn == "PNEUMONIA"
		pne = joinpath(trainPath,trn)
		for alltrainImg in readdir(pne)
			# println(alltrainImg)
			img = load(joinpath(pne,alltrainImg))
			img = Gray.(img)
			img = imresize(img,(28,28))
			# img = convert(Array{Float64},img)
			push!(trainData,img)
			push!(trainLabels,trn)
		end
	end
end

# ╔═╡ 4fe07572-6a41-48fb-919a-c1a3d3de76e3
trainData

# ╔═╡ aa9c8f34-7b9c-4563-a4d8-d21e595623c6
trainLabels

# ╔═╡ 53aa0ce7-327d-4a97-935c-8d835193787a
size(trainData)

# ╔═╡ 65fea638-14d6-41db-b89a-c3b8d23f12c3
size(trainLabels)

# ╔═╡ 4682ff98-7f45-4577-934d-8e99d918492a
md"### TESTING DATASETS"

# ╔═╡ d20d7ba4-73f5-4233-bd3d-bc990b572fb1
testData = []

# ╔═╡ 023251a1-e390-49c9-80d0-3be1cb2fe6f9
testLabels = []

# ╔═╡ a85f7f3e-cde2-4f5d-81c1-977cfb88fa51
test_dir = readdir(testPath)

# ╔═╡ 51cdb205-8ccf-43b7-9cd4-a88ea4676925
for trn in test_dir
	if trn == "NORMAL"
		nom1 = joinpath(testPath,trn)
		for alltestImg in readdir(nom1)
			img = load(joinpath(nom1,alltestImg))
			img = Gray.(img)
			img = imresize(img,(28,28))

			push!(testData,img)
			push!(testLabels,trn)
		end
	elseif trn == "PNEUMONIA"
		pne1 = joinpath(testPath,trn)
		for alltestImg in readdir(pne1)
			img = load(joinpath(pne1,alltestImg))
			img = Gray.(img)
			img = imresize(img,(28,28))
			
			push!(testData,img)
			push!(testLabels,trn)
		end
	end
end

# ╔═╡ 5b300e47-1b3d-4ebf-b538-5812df5aa9b3
testData

# ╔═╡ 757a9a28-d1c2-491d-b9c3-5f5876c2d47d
testLabels

# ╔═╡ 22700f99-d02f-45b7-aa5a-cfaab83defa9
size(testData)

# ╔═╡ 86e84e2c-8e93-47aa-871e-0fa1d727f0a2
size(testLabels)

# ╔═╡ 1aff8306-09ac-4eac-ba97-c1bcdc586eb3
md"### Data Transformation"

# ╔═╡ 054fc74e-9541-428f-a875-eaed07324d35
begin
	ytrain = onehotbatch(trainLabels,["NORMAL","PNEUMONIA"])
	ytest = onehotbatch(testLabels,["NORMAL","PNEUMONIA"])
end

# ╔═╡ b5db0574-9e5d-4527-a64c-b17e47bc702d
testLabels[600]

# ╔═╡ 3e442a4a-7997-4e5b-9327-d018e32d0217
onecold(ytest)[600]

# ╔═╡ d1d1bb68-7d3d-42eb-ab6a-25e654ddb4ce
preprocess(img) = Float64.(img)

# ╔═╡ f754ae66-ca5a-4af6-8757-b392b30c1023
reshaping(x) = reshape(x,(28,28,1))

# ╔═╡ 0e6f34de-ff2d-4c0f-8b5a-c814a9f6bb47
begin
	xtrain = [preprocess(p) for p in trainData]	
	xtrain = [reshaping(x_train) for x_train in xtrain] 
	xtrain = Flux.batch(xtrain)
end

# ╔═╡ 9d8e2d0a-aad0-4925-a2a8-fa79de604d98
begin
	xtest = [preprocess(p) for p in testData]	
	xtest = [reshaping(x_test) for x_test in xtest]
	xtest = Flux.batch(xtest)
end

# ╔═╡ 9f215f8c-5cbd-4084-8c5f-5b29ad8bfee0
md"### Loading Training Data into A DataLoader"

# ╔═╡ e24da66b-ee2c-4fed-8625-4b8d01500064
train_dataset = DataLoader((xtrain,ytrain), batchsize=128, shuffle=true)

# ╔═╡ ed39d297-afa0-4f86-a9b6-9c1ea99254e8
# test_dataset = DataLoader((xtest,ytest), batchsize=50, shuffle=true)

# ╔═╡ c0f2175f-7f1e-414d-9917-1cc2555fad3c
md"## Model"

# ╔═╡ 37359cac-cdd2-490b-8f21-febc212ab504
begin
	model = Chain(
            Conv((5, 5), 1=>6, relu),
            MaxPool((2, 2)),
            Conv((5, 5), 6=>16, relu),
            MaxPool((2, 2)),
            x -> reshape(x, :, size(x, 4)),
            Dense(*(4,4,16), 120, relu), 
            Dense(120, 84, relu), 
            Dense(84, 2)
          )
end

# ╔═╡ aebb832e-1978-42d8-a879-9e4ea7ac8ccf
# model.layers[7].W

# ╔═╡ cbc6457d-c257-4484-acb1-fdc883920272
md"### Training the Model"

# ╔═╡ 979a679a-71b2-46bd-ba69-6ae639d44217
md"Optimizer"

# ╔═╡ 0bcab94c-bc9f-4f1e-ac3a-f3590594fc92
opt = ADAM()

# ╔═╡ a691a112-151c-4ee7-a338-bcf3dcbd842e
md"Loss Function"

# ╔═╡ ac4c0b3f-0f34-48c5-8be7-5410303cf062
# soft max -> change output into probality and cross entropy -> how the neural network fit data
loss(x, y) = logitcrossentropy(model(x), y)

# ╔═╡ fd72c85a-34db-455b-955b-00d48942e861
md"Params"

# ╔═╡ 9666e19d-0815-416c-96e7-ccd98c566247
ps = params(model)

# ╔═╡ a64fd0f9-502d-4ceb-9ae2-f508cd8b55b9
    train_loss = Float64[]

# ╔═╡ 0d987a4a-e249-4038-9c40-69550dff1b88
function update_loss!()
        push!(train_loss, mean(loss(xtrain,ytrain)))
	
		@printf "Train loss = %.7f, " train_loss[end]
end

# ╔═╡ 796835b5-ac75-4f67-80bc-9251c49f4b6c
md"### Starting Training ....."

# ╔═╡ 62182c77-dcd3-41b8-bcce-f3cbac03a4fd
@epochs 30 train!(loss, ps,train_dataset, opt;
	cb = Flux.throttle(update_loss!, 5))

# ╔═╡ 6a3bfc45-e393-46ef-877f-740cba45aa64
plot(train_loss, xlabel="Iterations", title="Model Training", label="Train loss", lw=2, alpha=0.9)

# ╔═╡ e4d66bda-2568-40ac-92b4-3b48b0a8a51d
# onecold(model(xtest))

# ╔═╡ c3787f0e-bcfc-46e9-9b76-c245686181b4
# onecold(ytest)

# ╔═╡ 21efdbf0-e35a-42bb-bb53-549b2db90b68
# ytest

# ╔═╡ 8c1dc913-08b6-4aaa-b395-0df0cb2b30d8
md"## Evaluation The Model...."

# ╔═╡ b7fc04cd-34d9-4163-9589-70de5e286ad8
convertValue(x) = x == 2 ? 0 : x 

# ╔═╡ d1452b57-7799-4585-ace6-ffebe41b76c1
md"Converting Actual Values into 0 and 1"

# ╔═╡ 4aa6161d-df48-4829-b7d7-3d272a24fce3
begin
	testActualValues = onecold(ytest)
	testActualValues = [convertValue(xt) for xt in testActualValues]
end

# ╔═╡ 16bab347-fd65-447b-b6a5-37a36f3a8ae2
md"Converting predicted Values into True and false"

# ╔═╡ 308b7c54-6270-430b-afd3-88bc111deee7
begin
	testPredictValues = onecold(model(xtest))
	testPredictValues = testPredictValues.==1
end

# ╔═╡ 442571e5-d6a6-4135-990b-87206fa589a2
md"### Confusion Matrix"

# ╔═╡ e71565cc-cab8-48e9-93f3-69f59f004a86
size(ytest)

# ╔═╡ e483c932-a9ea-4493-be2f-3e7f8fad726d
cm  = ConfusionMatrix(testActualValues, testPredictValues)

# ╔═╡ 67fe33ed-3092-4332-aa1e-88d416826221
md"###### P i.e Positive == NORMAL"

# ╔═╡ dcb5a4e5-8f36-4a00-937b-e281a5217efe
md"###### N i.e Negetive == PNEUMONIA"

# ╔═╡ 85da7e1a-a19c-4089-82d5-821ae8e4d26b
confusion_matrix = [cm.tp cm.fn
					cm.fp	cm.tn]

# ╔═╡ 0e0d6e48-9595-450a-97d7-c9f7c48ff988
# heatmap(confusion_matrix, c = :thermal)

# ╔═╡ f5e39e54-3d50-4da6-a20d-6dd7cad45c86
header = ["","NORMAL","PNEUMONIA"]

# ╔═╡ 22c6084e-5f16-40f8-9298-ce49a55298b1
data = hcat(header[2:3],confusion_matrix)

# ╔═╡ 21b0f4be-a22a-4570-a326-0143b1e56612
with_terminal()do
	pretty_table(data;
		header = header,
		tf = tf_unicode_rounded)
end

# ╔═╡ 80d8c3e5-9ea3-45c4-b30f-7de14122b42a
md"## Accuracy"

# ╔═╡ ef790753-26cc-4310-9fba-62c12742ae57
md" Performance of a model. How close are predicted values to the actual values"

# ╔═╡ 5b75edf0-9d87-48cc-834f-9f1a5d0a5284
accuracy(testActualValues, testPredictValues)

# ╔═╡ 2a8d3794-03cb-4c2f-9775-557debafbec5
md"## Recall and Precision"

# ╔═╡ 7e9d4013-adfc-4286-863d-44c6bbbff077
md"##### Recall x = TP/(TP+FN) Precision y = TP/(TP+FP)"

# ╔═╡ 6ada2a6d-c595-428c-8050-be9a35d4270c
md"Recall Fraction-> All the times the actualValues were positive how many times the model was correct"

# ╔═╡ a9fb6c84-c4c0-4a2c-a651-4c59a3618773
md"Precision Fraction-> All the time model predict Pos how many times It was correct"

# ╔═╡ 1cd61509-eee1-4086-aafa-53b1e39df24c
md"#### Recall"

# ╔═╡ 0a6a4c4c-584a-441d-88eb-633b3da8b2a0
recall(testActualValues, testPredictValues)

# ╔═╡ 603c0fb9-a107-45ca-b0bd-cfb781a9598e
md"#### Precision"

# ╔═╡ 23a433ab-37b2-4b70-9c01-e25329f7db66
precision(testActualValues, testPredictValues)

# ╔═╡ 1b9d0ca9-b8ec-4549-93e4-d3fb997f20f5
# prcurve(testActualValues, testPredictValues)

# ╔═╡ 86f958eb-4fff-49cc-aa2d-d8c021f0dc28
    # test_loss = Float64[]
    # acc = Float64[]
    # accuracy(x, y, f) = mean(onecold(f(x)) .== onecold(y))
    # push!(test_loss, mean(loss(xtest,ytest)))
    # push!(acc, accuracy(xtest,ytest, model))
    # plot!(test_loss, label="Test loss", lw=2, alpha=0.9)
    # plot!(acc, label="Accuracy", lw=2, alpha=0.9)

# ╔═╡ Cell order:
# ╠═310e6cf0-cf7e-11eb-0eb5-b71bc39589af
# ╟─6e1cce7d-bdfa-4fbd-a62b-9e438d5bb1b5
# ╠═0636c81b-775c-46fe-a6f2-b09eaa2724f7
# ╠═61e6b54f-cd9c-4e2c-bb60-e1f104f34e4a
# ╠═56c8f34e-442d-4bba-9680-c6c3f502e749
# ╠═b3d39712-d633-4e5a-8658-3dad0cf652c6
# ╟─bc25b53a-af0b-498c-8f7a-5fc92d656cc3
# ╟─94ba899d-f4ad-49e6-9cb9-e21941037ae7
# ╠═b5c6b738-0ddd-4121-8070-08423b664168
# ╠═aeaf71df-8d82-477e-84b2-7873820b9fbd
# ╟─3844a430-4476-487d-aa0f-4e7cf43b266a
# ╠═ae1fad32-8601-4fc9-8217-d3b1d878de68
# ╠═8b5b2a5f-a953-4a29-873e-5f0248d1d001
# ╠═fc0f045a-852d-461c-a4fc-f8f4eb8b0b2b
# ╠═b8a5131b-ffcf-479c-ac3e-a9b64e3af858
# ╠═4fe07572-6a41-48fb-919a-c1a3d3de76e3
# ╠═aa9c8f34-7b9c-4563-a4d8-d21e595623c6
# ╠═53aa0ce7-327d-4a97-935c-8d835193787a
# ╠═65fea638-14d6-41db-b89a-c3b8d23f12c3
# ╟─4682ff98-7f45-4577-934d-8e99d918492a
# ╠═d20d7ba4-73f5-4233-bd3d-bc990b572fb1
# ╠═023251a1-e390-49c9-80d0-3be1cb2fe6f9
# ╠═a85f7f3e-cde2-4f5d-81c1-977cfb88fa51
# ╠═51cdb205-8ccf-43b7-9cd4-a88ea4676925
# ╠═5b300e47-1b3d-4ebf-b538-5812df5aa9b3
# ╠═757a9a28-d1c2-491d-b9c3-5f5876c2d47d
# ╠═22700f99-d02f-45b7-aa5a-cfaab83defa9
# ╠═86e84e2c-8e93-47aa-871e-0fa1d727f0a2
# ╟─1aff8306-09ac-4eac-ba97-c1bcdc586eb3
# ╠═054fc74e-9541-428f-a875-eaed07324d35
# ╠═b5db0574-9e5d-4527-a64c-b17e47bc702d
# ╠═3e442a4a-7997-4e5b-9327-d018e32d0217
# ╠═d1d1bb68-7d3d-42eb-ab6a-25e654ddb4ce
# ╠═f754ae66-ca5a-4af6-8757-b392b30c1023
# ╠═0e6f34de-ff2d-4c0f-8b5a-c814a9f6bb47
# ╠═9d8e2d0a-aad0-4925-a2a8-fa79de604d98
# ╟─9f215f8c-5cbd-4084-8c5f-5b29ad8bfee0
# ╠═e24da66b-ee2c-4fed-8625-4b8d01500064
# ╠═ed39d297-afa0-4f86-a9b6-9c1ea99254e8
# ╟─c0f2175f-7f1e-414d-9917-1cc2555fad3c
# ╠═37359cac-cdd2-490b-8f21-febc212ab504
# ╟─aebb832e-1978-42d8-a879-9e4ea7ac8ccf
# ╟─cbc6457d-c257-4484-acb1-fdc883920272
# ╟─979a679a-71b2-46bd-ba69-6ae639d44217
# ╠═0bcab94c-bc9f-4f1e-ac3a-f3590594fc92
# ╟─a691a112-151c-4ee7-a338-bcf3dcbd842e
# ╠═ac4c0b3f-0f34-48c5-8be7-5410303cf062
# ╟─fd72c85a-34db-455b-955b-00d48942e861
# ╠═9666e19d-0815-416c-96e7-ccd98c566247
# ╠═a64fd0f9-502d-4ceb-9ae2-f508cd8b55b9
# ╠═0d987a4a-e249-4038-9c40-69550dff1b88
# ╟─796835b5-ac75-4f67-80bc-9251c49f4b6c
# ╠═62182c77-dcd3-41b8-bcce-f3cbac03a4fd
# ╟─6a3bfc45-e393-46ef-877f-740cba45aa64
# ╟─e4d66bda-2568-40ac-92b4-3b48b0a8a51d
# ╟─c3787f0e-bcfc-46e9-9b76-c245686181b4
# ╟─21efdbf0-e35a-42bb-bb53-549b2db90b68
# ╟─8c1dc913-08b6-4aaa-b395-0df0cb2b30d8
# ╠═b7fc04cd-34d9-4163-9589-70de5e286ad8
# ╟─d1452b57-7799-4585-ace6-ffebe41b76c1
# ╠═4aa6161d-df48-4829-b7d7-3d272a24fce3
# ╟─16bab347-fd65-447b-b6a5-37a36f3a8ae2
# ╠═308b7c54-6270-430b-afd3-88bc111deee7
# ╟─442571e5-d6a6-4135-990b-87206fa589a2
# ╠═e71565cc-cab8-48e9-93f3-69f59f004a86
# ╠═e483c932-a9ea-4493-be2f-3e7f8fad726d
# ╟─67fe33ed-3092-4332-aa1e-88d416826221
# ╟─dcb5a4e5-8f36-4a00-937b-e281a5217efe
# ╠═85da7e1a-a19c-4089-82d5-821ae8e4d26b
# ╟─0e0d6e48-9595-450a-97d7-c9f7c48ff988
# ╠═f5e39e54-3d50-4da6-a20d-6dd7cad45c86
# ╠═22c6084e-5f16-40f8-9298-ce49a55298b1
# ╟─21b0f4be-a22a-4570-a326-0143b1e56612
# ╟─80d8c3e5-9ea3-45c4-b30f-7de14122b42a
# ╟─ef790753-26cc-4310-9fba-62c12742ae57
# ╠═5b75edf0-9d87-48cc-834f-9f1a5d0a5284
# ╟─2a8d3794-03cb-4c2f-9775-557debafbec5
# ╟─7e9d4013-adfc-4286-863d-44c6bbbff077
# ╟─6ada2a6d-c595-428c-8050-be9a35d4270c
# ╟─a9fb6c84-c4c0-4a2c-a651-4c59a3618773
# ╟─1cd61509-eee1-4086-aafa-53b1e39df24c
# ╠═0a6a4c4c-584a-441d-88eb-633b3da8b2a0
# ╟─603c0fb9-a107-45ca-b0bd-cfb781a9598e
# ╠═23a433ab-37b2-4b70-9c01-e25329f7db66
# ╟─1b9d0ca9-b8ec-4549-93e4-d3fb997f20f5
# ╟─86f958eb-4fff-49cc-aa2d-d8c021f0dc28
