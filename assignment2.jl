### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 310e6cf0-cf7e-11eb-0eb5-b71bc39589af
using Pkg;

# ╔═╡ 0636c81b-775c-46fe-a6f2-b09eaa2724f7
Pkg.activate(".")

# ╔═╡ 61e6b54f-cd9c-4e2c-bb60-e1f104f34e4a
begin
	using Flux;
	using MLDatasets: MNIST
	using Flux.Data: DataLoader
	using Flux: onehotbatch, logitcrossentropy, params, update!, train!, onecold, @epochs, onehot
	using Plots
	using PlutoUI
	using Printf
	using Statistics
end

# ╔═╡ b3d39712-d633-4e5a-8658-3dad0cf652c6
using Images,FileIO,ImageMagick,ImageIO

# ╔═╡ 6e1cce7d-bdfa-4fbd-a62b-9e438d5bb1b5
# Pkg.add("Flux")

# ╔═╡ 1300c5d2-59e4-4253-ab8e-141534eec163
# Pkg.add("MLDatasets")

# ╔═╡ 04264e56-669c-430e-b8cf-c3dbcf0be6f3
# Pkg.add("Images")

# ╔═╡ 22a4c64b-5705-4d5e-aa0a-aa3a32683b10
# Pkg.add("FileIO")

# ╔═╡ 46c304bf-338d-4585-a9e8-871bb98043f5
# Pkg.add("ImageMagick")

# ╔═╡ ee08d6d6-718b-4c0c-b1fb-cfb176b29063
# Pkg.add("ImageIO")

# ╔═╡ 5f2f4eb1-6493-4180-99d0-3dc844813b2b
# Pkg.add("ImageTransformations")

# ╔═╡ 7b278609-e765-496b-9691-67e5ccfcf390
# Pkg.add("Plots")

# ╔═╡ d1f2b9a2-a5b5-433b-93c4-cc8ffeeea162
# Pkg.add("PlutoUI")

# ╔═╡ bc25b53a-af0b-498c-8f7a-5fc92d656cc3
md"### Declaring Training and Testing Dataset"

# ╔═╡ e66bb0e9-2003-48f4-8e16-025a1de2a06a
# begin
# 	train_x, train_y = MNIST.traindata(Float32)
# 	test_x, test_y = MNIST.testdata(Float32)
# end

# ╔═╡ a66e00aa-a070-4441-b1fa-e78af012d49f
# begin
	# images = Flux.Data.MNIST.images(Float32)
# 	labels = Flux.Data.MNIST.labels()
# end

# ╔═╡ 94ba899d-f4ad-49e6-9cb9-e21941037ae7
md"#### File Path"

# ╔═╡ b5c6b738-0ddd-4121-8070-08423b664168
begin
	trainPath = "C:\\Users\\Isaac Makosa\\Desktop\\Assignment2\\chest_xray\\train"
	testPath = "C:\\Users\\Isaac Makosa\\Desktop\\Assignment2\\chest_xray\\test"
end

# ╔═╡ aeaf71df-8d82-477e-84b2-7873820b9fbd
labels=["NORMAL","PNEUMONIA"]

# ╔═╡ 3844a430-4476-487d-aa0f-4e7cf43b266a
md"### TRAINING DATASETS"

# ╔═╡ ae1fad32-8601-4fc9-8217-d3b1d878de68
train_dir = readdir(trainPath)

# ╔═╡ 8b5b2a5f-a953-4a29-873e-5f0248d1d001
trainData = []

# ╔═╡ fc0f045a-852d-461c-a4fc-f8f4eb8b0b2b
trainLabels = []

# ╔═╡ b8a5131b-ffcf-479c-ac3e-a9b64e3af858
for trn in train_dir
	if trn == "NORMAL"
		nom = joinpath(trainPath,trn)
		for alltrainImg in readdir(nom)
			# println(alltrainImg)
			# println(joinpath(nom,alltrainImg))
			img = load(joinpath(nom,alltrainImg))
			img = Gray.(img)
			img = imresize(img,(28,28))
			# img = convert(Array{Float64},img)
			push!(trainData,img)
			push!(trainLabels,trn)
		end
	elseif trn == "PNEUMONIA"
		pne = joinpath(trainPath,trn)
		for alltrainImg in readdir(pne)
			# println(alltrainImg)
			img = load(joinpath(pne,alltrainImg))
			img = Gray.(img)
			img = imresize(img,(28,28))
			# img = convert(Array{Float64},img)
			push!(trainData,img)
			push!(trainLabels,trn)
		end
	end
end

# ╔═╡ 4fe07572-6a41-48fb-919a-c1a3d3de76e3
trainData

# ╔═╡ aa9c8f34-7b9c-4563-a4d8-d21e595623c6
trainLabels

# ╔═╡ 53aa0ce7-327d-4a97-935c-8d835193787a
size(trainData)

# ╔═╡ 13bc39dc-cd6f-4882-808d-27348f13a119
trainData[1]

# ╔═╡ 65fea638-14d6-41db-b89a-c3b8d23f12c3
size(trainLabels)

# ╔═╡ 4682ff98-7f45-4577-934d-8e99d918492a
md"### TESTING DATASETS"

# ╔═╡ d20d7ba4-73f5-4233-bd3d-bc990b572fb1
testData = []

# ╔═╡ 023251a1-e390-49c9-80d0-3be1cb2fe6f9
testLabels = []

# ╔═╡ a85f7f3e-cde2-4f5d-81c1-977cfb88fa51
test_dir = readdir(testPath)

# ╔═╡ 51cdb205-8ccf-43b7-9cd4-a88ea4676925
for trn in test_dir
	if trn == "NORMAL"
		nom1 = joinpath(testPath,trn)
		for alltestImg in readdir(nom1)
			# println(alltrainImg)
			# println(joinpath(nom,alltrainImg))
			img = load(joinpath(nom1,alltestImg))
			img = Gray.(img)
			img = imresize(img,(28,28))
			# img = convert(Array{Float64},img)
			push!(testData,img)
			push!(testLabels,trn)
		end
	elseif trn == "PNEUMONIA"
		pne1 = joinpath(testPath,trn)
		for alltestImg in readdir(pne1)
			# println(alltrainImg)
			img = load(joinpath(pne1,alltestImg))
			img = Gray.(img)
			img = imresize(img,(28,28))
			# img = convert(Array{Float64},img)
			push!(testData,img)
			push!(testLabels,trn)
		end
	end
end

# ╔═╡ 22700f99-d02f-45b7-aa5a-cfaab83defa9
size(testData)

# ╔═╡ 86e84e2c-8e93-47aa-871e-0fa1d727f0a2
size(testLabels)

# ╔═╡ 054fc74e-9541-428f-a875-eaed07324d35
begin
	ytrain = onehotbatch(trainLabels,["NORMAL","PNEUMONIA"])
	# ytrain = Flux.batch(ytrain)
	ytest = onehotbatch(testLabels,["NORMAL","PNEUMONIA"])
	# ytest = Flux.batch(ytest)
end

# ╔═╡ d1d1bb68-7d3d-42eb-ab6a-25e654ddb4ce
preprocess(img) = Float64.(img)

# ╔═╡ f754ae66-ca5a-4af6-8757-b392b30c1023
reshaping(x) = reshape(x,(28,28,1))

# ╔═╡ 0e6f34de-ff2d-4c0f-8b5a-c814a9f6bb47
begin
	xtrain = [preprocess(p) for p in trainData]	
	xtrain = [reshaping(x_train) for x_train in xtrain] 
	xtrain = Flux.batch(xtrain)
end

# ╔═╡ 9d8e2d0a-aad0-4925-a2a8-fa79de604d98
begin
	xtest = [preprocess(p) for p in testData]	
	xtest = [reshaping(x_test) for x_test in xtest]
	xtest = Flux.batch(xtest)
end

# ╔═╡ 9f215f8c-5cbd-4084-8c5f-5b29ad8bfee0
md"### Loading Images into A DataLoader"

# ╔═╡ e24da66b-ee2c-4fed-8625-4b8d01500064
train_dataset = DataLoader((xtrain,ytrain), batchsize=128, shuffle=true)

# ╔═╡ ed39d297-afa0-4f86-a9b6-9c1ea99254e8
test_dataset = DataLoader((xtest,ytest), batchsize=50, shuffle=true)

# ╔═╡ c0f2175f-7f1e-414d-9917-1cc2555fad3c


# ╔═╡ 37359cac-cdd2-490b-8f21-febc212ab504
begin
	imgsize = (28,28,1)
	model = Chain(
            # x -> reshape(x, imgsize..., :),
            Conv((5, 5), 1=>6, relu),
            MaxPool((2, 2)),
            Conv((5, 5), 6=>16, relu),
            MaxPool((2, 2)),
            x -> reshape(x, :, size(x, 4)),
            Dense(prod((4,4,16)), 120, relu), 
            Dense(120, 84, relu), 
            Dense(84, 2)
          )
end

# ╔═╡ aebb832e-1978-42d8-a879-9e4ea7ac8ccf
# model.layers[7].W

# ╔═╡ ae020efa-9d3e-4135-ab5b-5662e653a8b4
	# plot(x,relu.(x),legend = false)

# ╔═╡ cbc6457d-c257-4484-acb1-fdc883920272
md"### Training the Model"

# ╔═╡ 979a679a-71b2-46bd-ba69-6ae639d44217
md"Optimizer"

# ╔═╡ 0bcab94c-bc9f-4f1e-ac3a-f3590594fc92
opt = ADAM()

# ╔═╡ a691a112-151c-4ee7-a338-bcf3dcbd842e
md"Loss Function"

# ╔═╡ ac4c0b3f-0f34-48c5-8be7-5410303cf062
# soft max -> change output into probality and cross entropy -> how the neural network fit data
loss(x, y) = logitcrossentropy(model(x), y)

# ╔═╡ fd72c85a-34db-455b-955b-00d48942e861
md"Params"

# ╔═╡ 9666e19d-0815-416c-96e7-ccd98c566247
ps = params(model)

# ╔═╡ 566c3a8f-90ed-487b-9005-9bda17571df8
md"#### Training ....."

# ╔═╡ a64fd0f9-502d-4ceb-9ae2-f508cd8b55b9
begin
    train_loss = Float64[]
    test_loss = Float64[]
    acc = Float64[]
    accuracy(x, y, f) = mean(onecold(f(x)) .== onecold(y))
end

# ╔═╡ 0d987a4a-e249-4038-9c40-69550dff1b88
function update_loss!()
        push!(train_loss, mean(loss(xtrain,ytrain)))
        push!(test_loss, mean(loss(xtest,ytest)))
        push!(acc, accuracy(xtest,ytest, model))
	
		@printf "Train loss = %.7f, " train_loss[end]
		@printf "Test loss = %.7f, " test_loss[end]
		@printf "Accuracy = %.7f\n" acc[end]	
end

# ╔═╡ 796835b5-ac75-4f67-80bc-9251c49f4b6c
md"Training ....."

# ╔═╡ 62182c77-dcd3-41b8-bcce-f3cbac03a4fd
@epochs 30 train!(loss, ps,train_dataset, opt;
	cb = Flux.throttle(update_loss!, 2))

# ╔═╡ 209177f6-ff2c-4c97-9d9c-13c63348358b
# update_loss!()

# ╔═╡ 6a3bfc45-e393-46ef-877f-740cba45aa64
begin
    plot(train_loss, xlabel="Iterations", title="Model Training", label="Train loss", lw=2, alpha=0.9)
    plot!(test_loss, label="Test loss", lw=2, alpha=0.9)
    plot!(acc, label="Accuracy", lw=2, alpha=0.9)
end

# ╔═╡ 2c6b2a97-1864-411e-8886-3d965c1d9bdc
# ;
# 	cb = Flux.throttle(update_loss!, 8)

# ╔═╡ b7098adf-91f6-4c94-8496-271a03ed1fb4
md"### Another Aproach"

# ╔═╡ 03a9a539-18b8-44a1-823c-775d2fe008e6
function make_minibatch(X, Y, idxs)
    X_batch = Array{Float32}(undef, size(X[1])..., 1, length(idxs))
    for i in 1:length(idxs)
        X_batch[:, :, :, i] = Float32.(X[idxs[i]])
    end
    Y_batch = onehotbatch(Y[idxs], 0:1)
    return (X_batch, Y_batch)
end

# ╔═╡ e0efdfb6-5c32-4f30-8953-058c6d5d4b4e
# begin
#     # here we define the train and test sets.
#     batchsize = 128
#     mb_idxs = partition(1:length(xTrain), batchsize)
#     train_set = [make_minibatch(xTrain, yTrain, i) for i in mb_idxs]
#     test_set = make_minibatch(xTest, yTrain, 1:length(x_test));
# end;

# ╔═╡ 374fcfe8-db31-4166-8fe8-1c539d3409fa
prepro(img) = vec(Float64.(img))

# ╔═╡ 89e8bcfb-7e89-4396-922a-15a7e62a9fd4
# function createBatch()
# 	xs = [prepro(img) for img in trainData]
# 	ys = [onehot(label,["NORMAL","PNEUMONIA"]) for label in trainLabels]
# 	return (Flux.batch(xs),Flux.batch(ys))
# end

# ╔═╡ 8c1dc913-08b6-4aaa-b395-0df0cb2b30d8
md"## Evaluation ...."

# ╔═╡ 4489d152-f245-4630-87a9-f7a13a30bc9c


# ╔═╡ Cell order:
# ╠═310e6cf0-cf7e-11eb-0eb5-b71bc39589af
# ╠═6e1cce7d-bdfa-4fbd-a62b-9e438d5bb1b5
# ╠═1300c5d2-59e4-4253-ab8e-141534eec163
# ╠═04264e56-669c-430e-b8cf-c3dbcf0be6f3
# ╠═22a4c64b-5705-4d5e-aa0a-aa3a32683b10
# ╠═46c304bf-338d-4585-a9e8-871bb98043f5
# ╠═ee08d6d6-718b-4c0c-b1fb-cfb176b29063
# ╠═5f2f4eb1-6493-4180-99d0-3dc844813b2b
# ╠═7b278609-e765-496b-9691-67e5ccfcf390
# ╠═d1f2b9a2-a5b5-433b-93c4-cc8ffeeea162
# ╠═0636c81b-775c-46fe-a6f2-b09eaa2724f7
# ╠═61e6b54f-cd9c-4e2c-bb60-e1f104f34e4a
# ╠═b3d39712-d633-4e5a-8658-3dad0cf652c6
# ╟─bc25b53a-af0b-498c-8f7a-5fc92d656cc3
# ╟─e66bb0e9-2003-48f4-8e16-025a1de2a06a
# ╟─a66e00aa-a070-4441-b1fa-e78af012d49f
# ╟─94ba899d-f4ad-49e6-9cb9-e21941037ae7
# ╠═b5c6b738-0ddd-4121-8070-08423b664168
# ╠═aeaf71df-8d82-477e-84b2-7873820b9fbd
# ╟─3844a430-4476-487d-aa0f-4e7cf43b266a
# ╠═ae1fad32-8601-4fc9-8217-d3b1d878de68
# ╠═8b5b2a5f-a953-4a29-873e-5f0248d1d001
# ╠═fc0f045a-852d-461c-a4fc-f8f4eb8b0b2b
# ╠═b8a5131b-ffcf-479c-ac3e-a9b64e3af858
# ╠═4fe07572-6a41-48fb-919a-c1a3d3de76e3
# ╠═aa9c8f34-7b9c-4563-a4d8-d21e595623c6
# ╠═53aa0ce7-327d-4a97-935c-8d835193787a
# ╠═13bc39dc-cd6f-4882-808d-27348f13a119
# ╠═65fea638-14d6-41db-b89a-c3b8d23f12c3
# ╟─4682ff98-7f45-4577-934d-8e99d918492a
# ╠═d20d7ba4-73f5-4233-bd3d-bc990b572fb1
# ╠═023251a1-e390-49c9-80d0-3be1cb2fe6f9
# ╠═a85f7f3e-cde2-4f5d-81c1-977cfb88fa51
# ╠═51cdb205-8ccf-43b7-9cd4-a88ea4676925
# ╠═22700f99-d02f-45b7-aa5a-cfaab83defa9
# ╠═86e84e2c-8e93-47aa-871e-0fa1d727f0a2
# ╠═054fc74e-9541-428f-a875-eaed07324d35
# ╠═d1d1bb68-7d3d-42eb-ab6a-25e654ddb4ce
# ╠═f754ae66-ca5a-4af6-8757-b392b30c1023
# ╠═0e6f34de-ff2d-4c0f-8b5a-c814a9f6bb47
# ╠═9d8e2d0a-aad0-4925-a2a8-fa79de604d98
# ╟─9f215f8c-5cbd-4084-8c5f-5b29ad8bfee0
# ╠═e24da66b-ee2c-4fed-8625-4b8d01500064
# ╠═ed39d297-afa0-4f86-a9b6-9c1ea99254e8
# ╠═c0f2175f-7f1e-414d-9917-1cc2555fad3c
# ╠═37359cac-cdd2-490b-8f21-febc212ab504
# ╠═aebb832e-1978-42d8-a879-9e4ea7ac8ccf
# ╠═ae020efa-9d3e-4135-ab5b-5662e653a8b4
# ╟─cbc6457d-c257-4484-acb1-fdc883920272
# ╟─979a679a-71b2-46bd-ba69-6ae639d44217
# ╠═0bcab94c-bc9f-4f1e-ac3a-f3590594fc92
# ╟─a691a112-151c-4ee7-a338-bcf3dcbd842e
# ╠═ac4c0b3f-0f34-48c5-8be7-5410303cf062
# ╟─fd72c85a-34db-455b-955b-00d48942e861
# ╠═9666e19d-0815-416c-96e7-ccd98c566247
# ╟─566c3a8f-90ed-487b-9005-9bda17571df8
# ╠═a64fd0f9-502d-4ceb-9ae2-f508cd8b55b9
# ╠═0d987a4a-e249-4038-9c40-69550dff1b88
# ╠═796835b5-ac75-4f67-80bc-9251c49f4b6c
# ╠═62182c77-dcd3-41b8-bcce-f3cbac03a4fd
# ╠═209177f6-ff2c-4c97-9d9c-13c63348358b
# ╠═6a3bfc45-e393-46ef-877f-740cba45aa64
# ╠═2c6b2a97-1864-411e-8886-3d965c1d9bdc
# ╟─b7098adf-91f6-4c94-8496-271a03ed1fb4
# ╟─03a9a539-18b8-44a1-823c-775d2fe008e6
# ╟─e0efdfb6-5c32-4f30-8953-058c6d5d4b4e
# ╟─374fcfe8-db31-4166-8fe8-1c539d3409fa
# ╟─89e8bcfb-7e89-4396-922a-15a7e62a9fd4
# ╟─8c1dc913-08b6-4aaa-b395-0df0cb2b30d8
# ╠═4489d152-f245-4630-87a9-f7a13a30bc9c
